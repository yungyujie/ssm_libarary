package cn.edu.cuit.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Book {
    private int id;
    private String name;
    private String author;
    private int tid;
    private double price;
    private String descri;
    private String photo;
    private Date pubdate;
    // 关联属性
    private Type type;
}
