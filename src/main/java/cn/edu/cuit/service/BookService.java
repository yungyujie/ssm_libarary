package cn.edu.cuit.service;

import cn.edu.cuit.entity.Book;
import cn.edu.cuit.entity.Type;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface BookService {
    @Transactional(readOnly = true)
    PageInfo<Book> findAllBooks(int pageNum, int pageSize);

    @Transactional(readOnly = true)
    List<Type> findAllType();
}
