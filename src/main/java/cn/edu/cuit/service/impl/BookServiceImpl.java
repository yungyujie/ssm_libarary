package cn.edu.cuit.service.impl;

import cn.edu.cuit.entity.Book;
import cn.edu.cuit.entity.Type;
import cn.edu.cuit.mapper.BookMapper;
import cn.edu.cuit.mapper.TypeMapper;
import cn.edu.cuit.service.BookService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper bookMapper;
    @Autowired
    private TypeMapper typeMapper;
    @Override
    public PageInfo<Book> findAllBooks(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Book> books = bookMapper.findAll();
        PageInfo<Book> pageInfo = new PageInfo<>(books);
        return pageInfo;
    }

    @Override
    public List<Type> findAllType() {
        return typeMapper.findAll();
    }
}
