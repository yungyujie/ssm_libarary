package cn.edu.cuit.controller;

import cn.edu.cuit.entity.Book;
import cn.edu.cuit.entity.Type;
import cn.edu.cuit.service.BookService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("book")
public class BookController {
    @Autowired
    private BookService bookService;
    @RequestMapping("list")
    public String list(Model model,
                       @RequestParam(required = false, defaultValue = "1") int pageNum,
                       @RequestParam(required = false, defaultValue = "1") int pageSize){
        PageInfo<Book> pageInfo = (PageInfo<Book>) bookService.findAllBooks(pageNum, pageSize);
        model.addAttribute("pageInfo", pageInfo);
        return "book/list";
    }

    @GetMapping("add")
    public String toAdd(Book book, Model model){
        List<Type> types = bookService.findAllType();
        model.addAttribute("types", types);
        return "book/add";
    }

    @PostMapping("add")
    public String doAdd(String name, String xx, Model model){
        model.addAttribute("name", name);
        model.addAttribute("xx", xx);
        return "book/add";
    }
}
