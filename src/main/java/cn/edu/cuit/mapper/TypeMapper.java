package cn.edu.cuit.mapper;

import cn.edu.cuit.entity.Type;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TypeMapper {
    @Select("select * from t_type where id = #{id}")
    Type findById(int id);

    @Select("select * from t_type")
    List<Type> findAll();
}
