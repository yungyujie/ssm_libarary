package cn.edu.cuit.mapper;

import cn.edu.cuit.entity.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookMapper {
    @Results(
            {
                    @Result(property = "type", column = "tid",
                            one = @One(select = "cn.edu.cuit.mapper.TypeMapper.findById"))
            }
    )
    @Select("select * from t_book")
    List<Book> findAll();
}
